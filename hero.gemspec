Gem::Specification.new do |s|
  s.name        = 'hero'
  s.version     = '0.0.1'
  s.executables << 'hero'
  s.date        = '2013-08-15'
  s.summary     = "S4DC"
  s.description = "A gem used by S4DC"
  s.authors     = ["Florian Schade"]
  s.email       = 'support@s4dc.com'
  s.files       = Dir["lib/**/*", "bin/*", "src/**/*", "LICENSE", "*.md"]
  s.homepage    = ''
  s.add_runtime_dependency "configliere"
  s.add_runtime_dependency "vagrant-wrapper"
  s.add_runtime_dependency "thor"
end