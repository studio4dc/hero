module Hero
  class Cli
    class Base < Thor
      map "-v" => "version"
      map "--version" => "version"

      desc "version", "print version"
      long_desc <<-D
      Print the Hero tool version
      D

      def version
        #Hero.info "Gitlab CLI version %s" % [Hero::VERSION]
      end

      ##init
      show_init = !Hero::configuration.hasHere?

      if show_init
        desc "init <path>", "init Hero"
        long_desc <<-D
      Init Hero
        D

        def init path
          Hero::mkdir path
          Hero::configuration.pathpackage = path
          Hero::configuration.create
        end
      end

      ##package
      show_package = Hero::configuration.hasHere?
      if show_package
        desc "package [SUBCOMMAND]", "perform an action on a package"
        long_desc <<-D
      Perform an action on a package. To see available subcommands use 'hero package help.'
        D

        subcommand "package", Hero::Command::Package
      end

      ##package
      show_server = Hero::configuration.hasHere?
      if show_server
        desc "server [SUBCOMMAND]", "perform an server action"
        long_desc <<-D
      perform an server action. To see available subcommands use 'hero server help.'
        D

        subcommand "server", Hero::Command::Server
      end
    end
  end
end