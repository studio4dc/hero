module Hero
  module Generator
    module Package
      class Existing < Base
        def add project_path
          Hero.mkdir project_path
        end
      end
    end
  end
end