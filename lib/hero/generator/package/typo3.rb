module Hero
  module Generator
    module Package
      class Typo3 < Base
        def add project_path
          Hero.mkdir project_path
          Hero::copy_from_to "#{Hero.configuration.pathsrc}/project/typo3/*", "#{project_path}/"
        end
      end
    end
  end
end