module Hero
  module Generator
    module Package
      class Wordpress < Base
        def add project_path
          Hero.mkdir project_path
          Hero::copy_from_to "#{Hero.configuration.pathsrc}/project/wordpress/*", "#{project_path}/"
        end
      end
    end
  end
end