module Hero
  module Generator
    module Package

      autoload :Typo3, 'hero/generator/package/typo3'
      autoload :Wordpress, 'hero/generator/package/wordpress'
      autoload :Existing, 'hero/generator/package/existing'

      class Base

        attr_accessor :settings, :package_name, :server_name, :local_package_folder, :server_package_folder, :php_timezone

        def initialize
          @settings = Hero.configuration.get_settings_for 'packages'
          @package_files = []
        end

        def add package_name, server_name, local_package_folder, package_framework
          package = Hero::Generator::Package::const_get(package_framework.camel_case).new
          package.add local_package_folder
          add_settings package_name, server_name, local_package_folder, package_framework
        end

        def delete package_name
          remove_settings package_name
        end

        private

        def add_settings package_name, server_name, local_package_folder, package_framework
          self.package_name = package_name
          self.server_name = server_name
          self.local_package_folder = local_package_folder
          self.server_package_folder = "/var/www/#{package_name}"
          self.php_timezone = 'Europe/Berlin'

          Hero::configuration.write_setting
        end

        def remove_settings package_name
          @settings.delete package_name
          Hero::configuration.write_setting
        end

        def package_name=(package_name)
          @settings[package_name] = {}
          @package_name = package_name
        end

        def server_name=(server_name)
          @settings[@package_name]['server_name'] = server_name
          @server_name = server_name
        end

        def local_package_folder=(local_package_folder)
          @settings[@package_name]['local_package_folder'] = local_package_folder
          @local_package_folder = local_package_folder
        end

        def server_package_folder=(server_package_folder)
          @settings[@package_name]['server_package_folder'] = server_package_folder
          @server_package_folder = server_package_folder
        end

        def php_timezone=(php_timezone)
          @settings[@package_name]['php_timezone'] = php_timezone
          @php_timezone = php_timezone
        end

      end
    end
  end
end