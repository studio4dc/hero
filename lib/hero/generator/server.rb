module Hero
  module Generator
    module Server
      autoload :Lamp, 'hero/generator/server/lamp'
      class Base

        attr_accessor :settings, :server_ip, :server_memory, :server_name

        def initialize
          @settings = Hero.configuration.get_settings_for 'server'
        end

        def check_dependencies
          VagrantWrapper.require_or_help_install(">= 1.2.7")
        end

        def add server_type
          server = Hero::Generator::Server::const_get(server_type.camel_case).new
          server.add
        end

        def configure server_ip, server_memory, server_name

          self.server_ip = server_ip.empty? ? '33.33.33.10' : server_ip
          self.server_memory = server_memory.empty? ? '2048' : server_memory
          self.server_name = server_name.empty? ? 'hero' : server_name
          Hero::configuration.write_setting
        end

        def start
          copy_runfile
          VagrantWrapper.new.execute "up"
        end

        def stop
          VagrantWrapper.new.execute "halt"
        end

        def delete
          VagrantWrapper.new.execute "destroy"
        end

        def login
          VagrantWrapper.new.execute "ssh"
        end

        private

        def server_ip=(server_ip)
          @settings['server_ip'] = server_ip
          @server_ip = server_ip
        end

        def server_memory=(server_memory)
          @settings['server_memory'] = server_memory
          @server_memory = server_memory
        end

        def server_name=(server_name)
          @settings['server_name'] = server_name
          @server_name = server_name
        end

        def copy_runfile
          Hero.copy_from_to "#{Hero.configuration.pathstorage}/Vagrantfile", "Vagrantfile", false
        end

        def remove_runfile
          Hero.delete "Vagrantfile"
        end

      end
    end
  end
end