module Hero
  module Generator
    module Server
      class Lamp < Base
        def initialize
          super
        end

        def add
          Hero::copy_from_to "#{Hero.configuration.pathsrc}/server/vagrant/*", Hero.configuration.pathstorage
        end
      end
    end
  end
end