require 'singleton'
module Hero

  def self.configuration
    return Configuration::Base.instance
  end

  module Configuration
    class Base
      include Singleton

      attr_accessor :path, :pathlib, :pathsrc, :pathcurrent, :pathstorage, :pathwebroot, :settings, :pathproject

      def initialize
        @path = File.dirname(File.dirname(File.dirname(__FILE__)))
        @pathlib = "#{@path}/lib"
        @pathsrc = "#{@path}/src"
        @pathcurrent = Dir.pwd
        @pathwebroot = "htdocs"
        @pathstorage = ".hero"
        @pathsettings = "#{@pathstorage}/configuration.yml"
        @settings = read_setting

        #puts @settings
      end

      def create
        skeleton
      end

      def hasHere?
        Hero::exists? ".hero"
      end

      def hasThere?
        Hero::exists? @pathstorage
      end

      def skeleton
        Hero::mkdir "#{@pathproject}/#{@pathstorage}/"
        Hero::mkdir "#{@pathproject}/#{@pathwebroot}/"
        Hero::copy_from_to "#{@pathsrc}/skeleton/hero/*", "#{@pathproject}/.hero/"
      end

      def read_setting
        return unless hasHere?
        configuration = YAML.load_file(@pathsettings)
        if !configuration
          configuration = {}
        end
        configuration
      end

      def write_setting
        File.open(@pathsettings, 'w+') { |f| f.write(@settings.to_yaml) }
      end

      def setup_setting_key key
        Settings.define :dest_time, :type => DateTime, :description => 'Arrival time'
        Hero::configuration.settings[key] = Hash.new
        Hero::configuration.settings[key]
      end

      def get_settings_for key
        @settings.has_key?(key) ? @settings[key] : setup_setting_key(key)
      end

    end
  end
end