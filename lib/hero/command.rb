module Hero
  module Command
    autoload :Package, 'hero/command/package'
    autoload :Server, 'hero/command/server'
  end
end