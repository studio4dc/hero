module Hero
  module Command
    class Package < Thor

      attr_accessor :package

      def initialize(*args)
        super
        @package = Hero::Generator::Package::Base.new
      end

      ##add
      desc "add [PROJECT_NAME] [PROJECT_URL]", "add a package"
      long_desc <<-LONGDESC
        Add a package.\n
        [PROJECT_NAME] = Name for the package\n
        [SERVER_NAME] = Url for the package (www.foo.loc)\n
      LONGDESC

      def add(package_name, server_name)
        framework = ask("Please enter Framework:", :limited_to => ["typo3", "wordpress", "existing"]) unless framework

        @package.add package_name, server_name, "#{Hero::configuration.pathwebroot}/#{package_name}", framework
      end


      ##delete
      desc "delete [PROJECT_NAME]", "delete a package"
      long_desc <<-D
        Delete a package. The only required parameter is the package name
      D

      def delete(package_name)
        @package.delete package_name
      end
    end
  end
end