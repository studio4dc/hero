module Hero
  module Command
    class Server < Thor

      attr_accessor :project

      def initialize(*args)
        super
        @server = Hero::Generator::Server::Base.new
        @server.check_dependencies
      end



      ##add
      desc "add", "add a server. Currently supportet <lamp>"
      long_desc <<-LONGDESC
        add a server.\n
        Currently supportet <lamp>
      LONGDESC

      def add
        server_type = ask("Please enter server_type:", :limited_to => ["lamp"])
        @server.add server_type
        configure
      end



      ##configure
      desc "configure", "set server params"
      long_desc <<-D
        set server params
      D

      method_option :server_ip, :type => :string, :default => '33.33.33.10', :desc => 'Server ip'
      method_option :server_memory, :type => :string, :default => 2048, :desc => 'Server Memory'
      method_option :server_name, :type => :string, :default => 'hero', :desc => 'Server Name'

      def configure
        server_ip = ask("Please enter server_ip:")
        server_memory = ask("Please enter server_memory:")
        server_name = ask("Please enter server_name:")

        @server.configure server_ip, server_memory, server_name
      end



      ##start
      desc "start", "start server"
      long_desc <<-D
        start server
      D

      def start
        @server.start
      end



      ##stop
      desc "stop", "stop server"
      long_desc <<-D
        stop server
      D

      def stop
        @server.stop
      end



      ##delete
      desc "delete", "delete server"
      long_desc <<-D
        delete server
      D

      def delete
        @server.delete
      end



      ##login
      desc "login", "login server"
      long_desc <<-D
        login server
      D

      def login
        @server.login
      end

    end
  end
end