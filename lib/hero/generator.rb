module Hero
  module Generator
    autoload :Package, 'hero/generator/package'
    autoload :Server, 'hero/generator/server'
    class Base
    end
  end
end