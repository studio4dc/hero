class String
  def camel_case
    return self if self !~ /_/ && self =~ /[A-Z]+.*/
    split('_').map { |e| e.capitalize }.join
  end
end

module Hero
  VERSION = "0.0.1"

  def self.copy_from_to from, to, glob = true
    from = glob ? Dir.glob(from) : from
    FileUtils.cp_r from, to
  end

  def self.mkdir path
    Dir.mkdir(path) unless exists? path
  end

  def self.delete path
    FileUtils.rm(path)
  end


  def self.exists? path
    File.exist? path
  end

end