require 'rubygems'
require 'yaml'

hero = YAML.load_file(".hero/configuration.yml")

web_apps = {}

hero['projects'].each do |k, v|
  web_apps[k] = {
    'server_name' => v['server_name'],
    'server_project_folder' => v['server_project_folder'],
    'local_project_folder' => v['local_project_folder'],
    'php_timezone' => v['php_timezone']
  }
end

Vagrant.configure('2') do |config|
  config.vm.box = 'Debian-Squeeze-6.0.7'
  config.vm.box_url = 'http://public.sphax3d.org/vagrant/squeeze64.box'

  config.vm.network :private_network, ip: hero['server']['server_ip']

  config.omnibus.chef_version = :latest

  config.vm.synced_folder 'htdocs', '/var/www'

  config.vm.provider :virtualbox do |provider|
    provider.gui = false
    provider.customize ['modifyvm', :id, '--memory', hero['server']['server_memory'], "--name", hero['server']['server_name']]
  end

  config.vm.provision :chef_solo do |chef|
    chef.cookbooks_path = '.hero/cookbooks'
    chef.add_recipe 'vagrant_main'

    chef.json = {
      'app' => {
        'web_apps' => web_apps,
      }
    }
  end
end